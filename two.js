// не позволяет в инпут вводить числа
(() => {
    let num = document.getElementById("noNumber");
    num.oninput = function (e) {
        num = e.target.value
        newNum = "";
        for (i = 0; i < num.length; i++) {
            if (isNaN(Number(num[i]))) {
                newNum = newNum + num[i];
            }
        }
        e.target.value = newNum;
    }
})();